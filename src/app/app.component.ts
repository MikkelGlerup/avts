import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../assets/CanvasJS.min';
import { shuffle } from './dataShuffler';
import * as SortingAlgo from './SortingAlgos';
import { MatSelectChange } from '@angular/material/select';

export interface Algorithm {
  value: string;
  viewValue: string;
}
let dataPoints = [];
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  algorithms: Algorithm[] = [
    { value: 'Insertion', viewValue: 'Insertion Sort' },
    { value: 'Quick', viewValue: 'Quick Sort' },
    { value: 'Bubble', viewValue: 'Bubble Sort' }
  ];


  selectedAlgorithm: string = '';
  dataPointsContainer: any[]

  async ngOnInit() {

    let y = 0;
    for (var i = 0; i < 1000; i++) {

      dataPoints.push({ y: i });
    }
    dataPoints = shuffle(dataPoints);
    let chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: false,
      exportEnabled: true,
      axisY: {
        valueFormatString: ' ',
        tickLength: 0,
        gridThickness: 0
      },
      axisX: {
        valueFormatString: ' ',
        tickLength: 0,
      },
      data: [{
        type: "column",
        dataPoints: dataPoints

      }]
    });
    // let testContainer = [];

    // for (let i = 0; i < dataPoints.length; i++) {
    //   testContainer.push(dataPoints[i].y);

    // }
    chart.render();
    dataPoints = dataPoints.sort((a, b) => (a.y > b.y) ? 1 : -1)
    for (let i = 0; i < dataPoints.length; i++) {
      dataPoints[i].x = dataPoints[i].y
      chart.render();
      await sleep(1);
    }
    
  }
  sortValues(event: Event) {
    switch (this.selectedAlgorithm.toLowerCase()) {
      case 'insertion':
        break;
      case 'quick':
        break;
      case 'bubble':
        break;
    }
  }
  selected(event: MatSelectChange) {
    this.selectedAlgorithm = event.value;;
  }
}
function sleep(ms = 0) {
    return new Promise(r => setTimeout(r, ms));
}

