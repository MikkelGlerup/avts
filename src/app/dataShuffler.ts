export function shuffle(array: number[]) {

    var currentIndex = array.length, temporaryValue, RandomIndex;
  
    while (0 !== currentIndex) {
      RandomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[RandomIndex];
      array[RandomIndex] = temporaryValue;
    }
    return array;
  }
  